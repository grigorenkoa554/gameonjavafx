import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.effect.Reflection;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

public class Controller {

    @FXML
    private Group root;

    @FXML
    private Label someLable;

    @FXML
    private TextField someText;

    @FXML
    private Button btn;
    @FXML
    private void click(ActionEvent event) {
        var some = btn.getText();
        someText.setCache(true);
        someText.setText("Reflections on JavaFX...");
        someText.setStyle("-fx-text-inner-color: red;");
        someText.setFont(Font.font("Verdana", FontWeight.BOLD, 30));

        Reflection r = new Reflection();
        r.setFraction(0.85f);

        someText.setEffect(r);

        btn.setText(some + "!" );
        btn.setFont(Font.font(40));
        ++someInt;
        someText.setText("Ha-" + someInt);
        int countSize = 20;

        someLable.setFont(Font.font(countSize));
        someLable.setText(someLable.getText() + "Ha");

        root.setOpacity(root.getOpacity() - 0.05);
        btn.setStyle("-fx-background-color: green; ");
    }

    private static int someInt = 0;
}
